## Sıkıştırma ve Açma

`unzip`, `gunzip`, `bunzip2`, `unrar`  vb  # ile sıkıştırılmış dosyalar açılır veya sıkıştırılır, 
 komut yoksa apt ile kur. ör: sudo apt-get install rar ;  sudo apt-get install unrar

* `zip abc.zip abc` # abc dosyasını sıkıştırır abc.zip yapar
* `unzip abc.zip` # abc.zip dosyasını açar. 

* `gzip abc` # abc.gz yapar; abc yi siler
* `gunzip abc.gz` # açar


* `bzip2 abc` # abc dosyasını sıkıştırır abc.bz2 yapar
* `bunzip2 abc.bz2` # abc.bz2 dosyasını açar. 

* `rar a abc.rar abc` # abc dosyasını sıkıştırır.
* `unrar x abc.rar` # açar.




