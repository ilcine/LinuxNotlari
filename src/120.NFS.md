## NFS 

> NFS (Network File System) mount komutu ile veya /etc/fstab ta tanım yapılır. 

* `mount -t nfs 94.177.187.77:/disk1/home /tmp/home` # Uzak sistemdeki disk1/home dizinini kendi sistemimizdeki /tmp/home alanına bağlamak için mount kullanılır.
* `umount /tmp/home` # ile mount işleme durdurulur

* NFS; Dizin paylaşacak 192.168.0.1 Sunucu için:
    `/etc/exports` dosyasına `/home/emr 192.168.0.2(ro,sync)` yaz; anlamı:192.168.0.2 ip nolu suncu /home/emr dizinine okuma yetkisi virilir.
    /etc/init.d/nfs start  # nfs sunucusu başlatılır.
    exportfs -ra # komutu işlemi aktif hale getirir; reboot te kullanılır; 
* NFS; Dizini Görecek 192.168.0.2 Sunucu için:
    `mount -t nfs  192.168.0.1:/home/emr`  // dizini paylaşacak sunucunun ip nosu
    `showmount -e`  # neler mount edilmiş bakılır.  
    `df` ile de bağlanılan partition görülür.
