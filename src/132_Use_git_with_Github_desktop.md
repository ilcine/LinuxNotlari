Use git with  Github desktop
======================


1. On Github destop (first install github desktop)
   1. First definition (if the project has already been created on Gitlab)
      1. / [File]  / [Clone repositor] / 
      2. [URL] tab; ex: “https://gitlab.com/ilcine/laravelenvset.git”
      3. [Local Path] tab; ex: “C:\Users\emr\Documents\GitLab”
      4. [Clone] enter
   1. Receive project from gitlab site to local PC
      1. [Current repository] menu; see “laravelenvset” and check it
      2. Check [repository] / [Repository setting] and see project remote section and save if necessary
      3. [Fetch origin] menu; it receive
   1. Put project to Gitlab from local PC
      1. Chose "Current repository"  and see project on  [Other]  menu
      2. Make changes to the project directory (ex:C:\Users\emr\Documents\GitLab\xxx)
      3. [Fetch origin] ; and see changed
      4. [Summary (request)] and [Desciption] write short note
      5. [Commit to Master] button press
      6. [Push origin] press ; icon return and  it push to Gitlab 
   1. Delete Project
      1. [Repository] / [Remove] settings.. advanced setting... remove project
1. On Gitlab; first register and login
   1. Create project
      1. / Project / your project / new project [ buttonu] press
   1. See Project
      1. / [project] / [your project] ;  see  project
   1. Remove project
      1. Open project // project in the menu on the left.
      2. Setting (In the left sidebar)  
      3. General
      4. Advanced Setting (Click on Expand)
      5. Remove Project (Bottom of the Page)
      6. Confirm (By typing project name and press Confirm button)
   1. Publishing for public  (Default private)
      1. Open project  and Setting menu
      2. General
      3. Visibility, project features, permissions/[expand]/
      4. Project visibility [Public]