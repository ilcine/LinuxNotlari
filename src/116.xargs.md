#  xargs

Üzerine aldığı veriyi diğer komuta tek tek taşır

help: xargs --help ;  

ex:

`echo "sayi" | xargs echo "birinci"`   # output `birinci sayi`

`echo "sayi" | xargs -i echo {} "birinci"` # output `sayi birinci`  # i parametresi replace yapar. yer değiştiri. 

`grep -rl 'Basit' ./ | xargs sed -i 's/Basit/Master/g'` # Basit kelimesini alt dizinlerdeki dosyalarda arar ve Basit kelilemrini Master yapar.


