## Network; Ağ işlemleri

* `netstat -a` # listen,estableshed; giren çıkan paketler port noları listelenir.
* `netstat -tunap` # tcp/udp portların uygulama isimleriyle listelenir.
* `route` # Yönlenmeleri gösterir
* `hostname` # Hostun adını gösterir
* `hostname -d` # Domain i gösterir.
* `hostname www.ilcin.name.tr` # Hostu ve domain i değiştirir 
* `hostname -a`  # Verilmiş alias adları gösterir
* `cat /etc/resolv.conf`  # Dns çözümlemesi yapan hostları gösterir
* `cat /etc/network/interfaces` #  Atanmış ip, mask, dns leri gösterir
* `ifconfig` #Atanmış Interface leri ethernet,wifi vb gösterir
* `ifconfigi eth0:1  192.168.0.2` # eth0 a ikinci bir ip daha ver; ( bir ethernete birçok ip atanabilir)   
* `ping www.google.com`  # Google'e erişiliyormu sinyali gönderir.
* `fping` # Alternatif ping; apt ile kur; tüm network class lardaki ip lerin, alive(canlı) olup olmadığı vb görür
* `dig ilcin.name.tr`  # gets DNS information for domain
* `dig -x www.ilcin.name.tr`  # reverses lookup host varmı
* `dig uludag.edu.tr a +short` # uludag a dns kaydı sorgusu
* `nslookup -q=a www.ilcin.name.tr`  # default dns e sor, host'un A kaydı varmı sor
* `nslookup -q=any ilcin.name.tr 8.8.8.8`  # ilcin.name.tr domain'i, google dns te tanımlımı
* `nslookup -q=any uludag.edu.tr 195.175.39.49` # ttnet dns te kaydı varmı (tr den sor)
* `ip route show` // ipv4 durumu
* `netstat -nr -6`  // ipv6 durumu
* `ip -6 route show` // ipv6 route durumu
* `nmap -v -A localhost` // Port ları tarar.
* `nmap -sS -sU -PN  localhost` # Sunucunun syn,ack gibi connectionları ve  port kullanımlarını gösterir
* `nmap -sS google.com.tr`  # Google sunucusundaki açık portları gösterir.

### reverse ssh

1. emr@PrivateHost 192.168.1.1>`ssh 65535:localhost:22 emr@PublicHost.ilcine.name.tr` # Remote connection with port 65535 ; see port `netstat -ntl| grep 65535`
2. emr@PublicHost. ilcin.name.tr>`ssh emr@localhost -p 65535` # Reverse connection; You will see "PrivateHost 192.168.1.1>" in the console;  reverse connection, nat and firewall passes.

### reverse http; ubuntu 18.04; apache2

1. emr@PublicHost. ilcin.name.tr> `echo "GatewayPorts yes" >> /etc/ssh/sshd_config` and `/etc/init.d/ssh restart`
2. emr@PrivateHost_192.168.1.1> `sed -i 's/Listen 80/Listen 8088/g' /etc/apache2/ports.conf` and `/etc/init.d/apache2 restart`
3. Any PC browser> `http://PublicHost.ilcin.name.tr:8088` # Accesses PrivateHost via the PublicHost tunnel.

### Authentication without password using OpenSSH Key

1. emr@PrivateHost> `ssh-keygen -t rsa -b 1048 -f ~/.ssh/id_rsa_my_cert` #  Press Enter to PassPhrase
2. emr@PrivateHost> `ssh-copy-id -i ~/.ssh/id_rsa_my_cert.pub emr@PublicHost.ilcin.name.tr` # Enter emr@PublicHost.ilcin.name's password; wrote to "~/.ssh/authorized_keys" file on "emr@PublicHost.ilcin.name.tr" server
3. emr@PrivateHost> `ssh -i -f ~/.ssh/id_rsa_my_cert emr@PublicHost.ilcin.name.tr` # You will see "emr@PublicHost.ilcin.name.tr>" prompt

