# Linux Operation System:

The most basic code of computers is
kernel (kernel) with GNU General Public License
developed and presented with a free use right
operating system software project.
There are many linux versions. (Debian, Redhat, Centos, Ubuntu, Suse, Fedora etc.).
Command codes with small changes
It's like Unix systems. This documentation was taken as Ubuntu.

(Bilgisayarlerin en temel kodu, özü durumundaki
kernel(çekirdek)'in GNU Genel Kamu Lisansı ile 
geliştirilen ve özgür kullanım hakkı ile sunulan bir 
işletim sistemi yazılım projesidir. 
Birçok linux sürümü vardır. (Debian, Redhat, Centos, Ubuntu, Suse, Fedora vb).
Komut kodları küçük değişiklilerle
Unix sistemleri gibidir. Bu notlarda Ubuntu örnek alınmıştır.)
  
> _Emrullah İLÇİN_


* [010.Help Yardım Komutları Kullanımı:](/src/010.help.md)
* [020.Man Komutların Menulerini Görme:](/src/020.man.md)
* [030.Kullanılan Simge,işaret ve Aciklamalar:](/src/030.Simge.ve.Aciklamalar.md)
* [031.Dizin Yapısı:](/src/031.Dizin.md)
* [032.Kullanici Yetkileri:](/src/032.Kullanici.Yetkileri.md)
* [033.Tarih, Takvim, Hesap Makinası Kullanımı vb:](/src/033.Tarih.takvim.hesap.vb.md)
* [034.Path Yol Tanımı:](/src/034.Path.md)
* [040.Dosya Listeleme, Yaratma, Silme:](/src/040.Dosya.Listeleme.Yaratma.Silme.md)
* [050.Dosya, Dizin Yetki ve Sahiplik:](/src/050.Dosya.Dizin.Yetki.ve.Sahiplik.md)
* [060.Dosya Tipini Görme, Degistirme:](/src/060.Dosya.Tipi.Gorme.Degistirme.md)
* [070.Find Dosya Arama:](/src/070.Find.Dosya.Arama.md)
* [080.Grep İçerik Arama:](/src/080.Grep.Icerik.Arama.md)
* [081.Dosya Sıkıştırma:](/src/081.Dosya.Sıkıstirma.md)
* [082.Sort ve Uniq Kullanımı:](/src/082.Sort.Uniq.md)
* [083.Cut command:](/src/083_cut_command.md) 
* [090.Diff Dosyalar Arasındaki Farkliligi Bulma:](/src/090.Diff.Farkliligi.Bulma.md)
* [100.Dosya Taşıma, Kopyalama:](/src/100.Dosya.Tasima.Kopyalama.md)
* [110.Dosya Transferi scp,ftp,wget,curl:](/src/110.Dosya.Transferi.scp.ftp.wget.curl.md)
* [111.Editorler:](/src/111.Editor.md)
* [113.Sed Streem Editör:](/src/113.Sed.md)
* [114.Awk Yığın Veri İşleme Programı/Editörü:](/src/114.Awk.md)
* [115.Process Çalışan İşlerin Görülmesi:](/src/115.Process.md)
* [120.NFS Network File System:](/src/120.NFS.md)
* [130.TAR Tape Arsiv:](/src/130.tar.md)
* [131.GITLAB Depo Kullanımı:](/src/131.Gitlab.md)
* [132.Use GIT with Github desktop:](/src/132_Use_git_with_Github_desktop.md)
* [140.Networking:](/src/140.Networking.md)
* [150.Crontab Zamanlaştırılmış Görevler:](/src/150.Crontab.md)
* [160.Logrotate Log Yönetimi:](/src/160.Logrotate.md)
* [170.Quota Kota Tanamlama:](/src/170.Quota.md)
* [180.Clock Saatin Ayarlanması:](/src/180.Clock.set.md)
* [190.Security Güvenlik İçin Gereklilikler:](/src/190.Security.md)
* [200.Bash Örnekleri:](/src/200.Bash.md)
